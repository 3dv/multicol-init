import sys
import os
import shutil
import argparse
from pathlib import Path

parser = argparse.ArgumentParser()
parser.add_argument('multicol_binary', help='path to a compiled MultiCol binary')
parser.add_argument('config_dir', help='path to a directory with MultiCol configuration')
parser.add_argument('data_dir', help='path to a directory with the dataset')
parser.add_argument('--first_frame', type=int, nargs='?', default=1, 
                    help='index of the first frame to process')
parser.add_argument('--last_frame', type=int, nargs='?', default=201,
                    help='index of the last frame to process')
parser.add_argument('--frames_per_init', type=int, nargs='?', default=50,
                    help='the amount of frames given to multicol to initialize')
parser.add_argument('--out_dir', nargs='?', default='init',
                    help='the directory to output trajectories and clouds into')
parser.add_argument('--num_repeats', type=int, nargs='?', default=1,
                    help='the number of runs through the whole dataset')
parser.add_argument('--dry_run', action='store_true')
args = parser.parse_args()

config_dir = Path(args.config_dir)
settings_template_fname = str(config_dir / 'MultiColSettings_template.yaml')
vocab_fname = str(config_dir / 'small_orb_omni_voc_9_6.yml')

for it in range(args.num_repeats):
    out_dir = Path(args.out_dir) / Path(f'{it:02}')
    settings_dir = Path('settings_files')
    if args.dry_run:
        print(f'mkdir {str(settings_dir)}')
    else:
        os.makedirs(str(settings_dir), exist_ok=True)

    for init_frame in range(args.first_frame, args.last_frame - args.frames_per_init + 2, 
                            args.frames_per_init):
        cur_last_frame = init_frame + args.frames_per_init - 1
        cur_name = f'{init_frame:04}_to_{cur_last_frame:04}'
        cur_dir_p = out_dir / cur_name
        cur_dir = str(cur_dir_p)
        if args.dry_run:
            print(f'mkdir {cur_dir}')
        else:
            os.makedirs(cur_dir, exist_ok=True)

        settings_fname = str(settings_dir / f'{cur_name}.yaml')
        if args.dry_run:
            print(f'cp {settings_template_fname} {settings_fname}')
            print(f'append to settings')
        else:
            shutil.copyfile(settings_template_fname, settings_fname)
            settings_file = open(settings_fname, 'a')
            print(f'traj.StartFrame: {init_frame}', file=settings_file)
            print(f'traj.EndFrame: {cur_last_frame}', file=settings_file)
            settings_file.close()


        command = f'{args.multicol_binary} {vocab_fname} {settings_fname}' \
                  f' {args.config_dir} {args.data_dir}'
        if args.dry_run:
            print(f'exec: {command}')
        else:
            os.system(command)

        cur_out_traj_fname = str(cur_dir_p / f'{cur_name}_poses.txt')
        cur_out_points_fname = str(cur_dir_p / f'{cur_name}_points.txt')
        if args.dry_run:
            print(f'cp MKFTrajectory.txt {cur_out_traj_fname}')
            print(f'cp points.txt {cur_out_points_fname}')
        else:
            shutil.copyfile('MKFTrajectory.txt', cur_out_traj_fname)
            shutil.copyfile('points.txt', cur_out_points_fname)
